\documentclass[%aspectratio=169, 
                11pt]{beamer} 


% Definições do tema
\mode<presentation>{
\usetheme{Boadilla} %Boadilla
}
\usecolortheme[RGB={0,128,197}]{structure}

%%% PACOTES
\usepackage[alf]{abntex2cite} % citações ABNT
\usepackage{lmodern}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{mathptmx}
\usepackage{siunitx}
\usepackage{threeparttable} % Para poder adicionar notas em tabelas
%\usepackage{caption} % Para poder adicionar notas em figuras, NÃO FUNCIONANDO NO DEBUG
\usepackage{dcolumn}
\usepackage{xcolor} % Para cores em linhas de tabelas
\usepackage{colortbl} % Para cores em linhas de tabelas


%%% COMANDOS
% Criando novas cores
\newcommand{\mc}[2]{\multicolumn{#1}{c}{#2}}
\definecolor{Gray}{gray}{0.90}
\definecolor{LightCyan}{rgb}{0.88,1,1}

% Comando novo para o dcolumn
\newcolumntype{L}[1]{D{.}{#1}{1,3}}
\newcommand\CommaCell[1]{%
  \multicolumn{1}{L{,}}{#1}%
}



% Definições para o primeiro slide

\title[] 
{O mercado financeiro:}
\subtitle[] 
{papel do Estado e o SFN}


\author[Wlademir Ribeiro ~Prates]
{Wlademir Ribeiro ~Prates \inst{1}}
\institute[ ]
{
	{1}
	Doutor em finanças UFSC \and 
}
%\newcommand{\dateToday}{www.wrprates.com}

\date[Florianópolis]{}



% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
\AtBeginSubsection[]
{
  \begin{frame}<beamer>{Índice}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}






\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Índice}
  \tableofcontents
  % You might wish to add the option [pausesections]
\end{frame}


\section{Papel do Estado na economia}

\subsection{Contextualização}

\begin{frame}{Da mão invisível para um Estado regulador}
\begin{itemize}
\item \textbf{Adam Smith:} mão invisível;
\item \textbf{Economistas neoclássicos:} aprefeiçoaram o liberalismo, construíram modelos quantitativos e criaram premissas, como a hipótese do \textit{homo economicus}
\begin{itemize}
\item Menger, Walras, Pareto, Marshall, Fisher
\end{itemize}
\item \textbf{Crise de 1929 e Keynes:} a necessidade da intervenção estatal de forma a \alert{regular o mercado e evitar crises}. 
\item Surgiram as \textbf{políticas econômicas.}
\end{itemize}
\footnotesize
\begin{quote}
``A euforia das demissões reduzem a demanda efetiva e pioram ainda mais o ciclo, sendo necessáaria a intervenção do Estado''.
\end{quote}
\end{frame}

\subsection{Políticas econômicas}

\begin{frame}{Políticas econômicas}
\begin{itemize}
\item \textbf{Política monetária:} controla a oferta de moeda (inflação e taxa de juros);
\item \textbf{Política fiscal:} controle da arrecadação e dos gastos;
\item \textbf{Política cambial:} administra a taxa de câmbio.
\end{itemize}
\end{frame}

\begin{frame}{Política monetária}
O Banco Central (BACEN ou BC) é órgão que executa a política monetária, por meio de:
\begin{enumerate}
\item \textbf{Depósito compulsório:} obriga os bancos a depositarem parte dos recursos captados de clientes (depósitos à vista – CC, a prazo – CDB) ou poupança em uma conta do BC;
\item \textbf{Redesconto:} um ``socorro'' aos bancos. É um empréstimo de liquidez do BC aos bancos;
\item \textbf{Open market:} o próprio BC atua no mercado comprando e vendendo títulos públicos;
\item \textbf{Seleção de crédito:} normas específicas que geram restrições ao livre funcionamento do mercado.
\end{enumerate}
\end{frame}

\begin{frame}{Política fiscal}
Balanço entre o que é arrecadado (receitas públicas) e o que é gasto (despesas públicas). Impacta na \alert{carga tributária}.
\begin{itemize}
\item \textbf{Aumentar receitas:} \textit{reforma tributária} que melhore a capacidade de arrecadar.
\item \textbf{Reduzir despesas:} \textit{reforma administrativa} para reduzir salários e custos.
\end{itemize}
\end{frame}


\begin{frame}{Exemplos de execução da política fiscal}
\textbf{Problema:} governo \alert{gasta mais do que arrecada}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{itemize}
\item \textbf{Solução:} recorrer ao endividamento público, pela emissão de títulos públicos.
\item \textbf{Efeito:} isso prejudica o fomento a capacidade produtiva.
\end{itemize}
\end{column}
\begin{column}{0.5\textwidth}  
\begin{center}
\includegraphics[width=\linewidth]{images/fiscal_1.pdf}
\end{center}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Exemplos de execução da política fiscal}
\textbf{Problema:} \alert{crescimento da dívida} com emissão de títulos
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{itemize}
\item \textbf{Solução:} os ativos da União devem crescer junto com a dívida;
\item \textbf{Exemplo:} capitalização no BB por parte da União aumenta a
dívida, mas também aumenta os ativos da União.
\end{itemize}
\end{column}
\begin{column}{0.5\textwidth}  
\begin{center}
\includegraphics[width=\linewidth]{images/fiscal_2.pdf}
\end{center}
\end{column}
\end{columns}
\end{frame}



\begin{frame}{Política cambial}
Administração da \alert{taxa de câmbio flutuante}, determinada pela \textbf{oferta e demanda} -- investimentos estrangeiros, taxa de importação/exportação.
\begin{itemize}
\item Quando as flutuações estão acima do normal, o BC interfere (compra ou vende grandes quantias de dólares)
\begin{itemize}
\item \textbf{impedir queda excessiva:} BC compra; 
\item \textbf{impedir alta excessiva:} BC vende.
\end{itemize}
\item Mas isto gera um impacto na política monetária, porque o excesso de moedas na economia gera inflação.
\end{itemize}
\end{frame}


\section{Sistema Financeiro Nacional}
\subsection{Mercados financeiro}

\begin{frame}{Função de um mercado financeiro}
\textbf{Reunir poupadores e tomadores} de forma mais eficiente do que por meio de empréstimos diretos.
\newline
\newline
Vantagens da existência de intermediários:
\begin{itemize}
\item economias de escala;
\item diversifição do risco; 
\item especialização.
\end{itemize}
\end{frame}

\subsection{Estrutura do SFN}

\begin{frame}{Estrutura do SFN}
\begin{center}
\includegraphics[width=\linewidth]{images/estrutura_sfn.pdf}
\end{center}
\footnotesize
\href{https://www.bcb.gov.br/pre/composicao/composicao.asp}{Site BACEN}.
\end{frame}


\begin{frame}{Operadores do SFN -- Bancos}
\begin{center}
\includegraphics[width=\linewidth]{images/instituicoes_1.pdf}
\end{center}
\end{frame}

\begin{frame}{Operadores do SFN -- Outras instituições}
\begin{center}
\includegraphics[width=\linewidth]{images/instituicoes_2.pdf}
\end{center}
\end{frame}

\subsection{Fundo Garantidor de Crédito}

\begin{frame}{O Fundo Garantidor de Crédito}
É uma associação civil sem fins lucrativos, com personalidade jurídica de direito privado.
\begin{itemize}
\item \textbf{Limite de cobertura:} R\$ 250.000,00
\item \textbf{Contribuição:} percentual sobre o saldo das contas que são objetos de garantia (0,0125\% a.m.)
\item \textbf{Alguns depósitos garantidos:}
\begin{itemize}
\item Depósitos de poupança;
\item Depósitos a prazo, com ou sem emissão de certificado (CDB e RDB);
\item Letras de crédito imobiliário (LCI) e do agronegócio (LCA);
\item \href{http://www.fgc.org.br/garantia-fgc/sobre-a-garantia-fgc}{entre outros} \dots
\end{itemize}
\end{itemize}
\end{frame}

%%%%
%Quem é o investidor?
%A origem dos mercados financeiros e o papel da intermediação.
%Sistema Financeiro Nacional: o papel das instituições.
%Conceitos básicos para o mundo dos investimentos – SELIC, CDI, políticas (econômica, fiscal)


\end{document}