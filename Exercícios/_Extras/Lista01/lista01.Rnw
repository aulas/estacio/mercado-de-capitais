
% oneside = apenas frente
% twocolumn = artigo em duas colunas
\documentclass[article,10pt,oneside,a4paper,twocolumn,english]{abntex2}  

% ---
% PACOTES
% ---

% ---
% Pacotes fundamentais 
% ---
\usepackage{cmap}				% Mapear caracteres especiais no PDF
\usepackage{lmodern}			% Usa a fonte Latin Modern
\usepackage[T1]{fontenc}		% Selecao de codigos de fonte.
\usepackage[utf8]{inputenc}		% Codificacao do documento (conversão automática dos acentos)
\usepackage{indentfirst}		% Indenta o primeiro parágrafo de cada seção.
\usepackage{nomencl} 			% Lista de simbolos
\usepackage{color}				% Controle das cores
\usepackage{graphicx}			% Inclusão de gráficos
% ---
		
% ---
% Pacotes adicionais, usados apenas no âmbito do Modelo Canônico do abnteX2
% ---
\usepackage{lipsum}				% para geração de dummy text
% ---
		
% ---
% Pacotes de citações
% ---
\usepackage[brazilian,hyperpageref]{backref}	 % Paginas com as citações na bibl
\usepackage[alf]{abntex2cite}	% Citações padrão ABNT
% ---

% ---
% Configurações do pacote backref
% Usado sem a opção hyperpageref de backref
\renewcommand{\backrefpagesname}{Citado na(s) página(s):~}
% Texto padrão antes do número das páginas
\renewcommand{\backref}{}
% Define os textos da citação
\renewcommand*{\backrefalt}[4]{
	\ifcase #1 %
		Nenhuma citação no texto.%
	\or
		Citado na página #2.%
	\else
		Citado #1 vezes nas páginas #2.%
	\fi}%
% ---


% ---
% Informações de dados para CAPA e FOLHA DE ROSTO
% ---
\titulo{Lista de exercícios -- Mercado de capitais}
\autor{Professor: Wlademir Ribeiro Prates, Dr.}
\local{Faculdade Borges de Mendonça -- Pós em Finanças}
%\data{2015}
% ---

% ---
% Configurações de aparência do PDF final

% alterando o aspecto da cor azul
\definecolor{blue}{RGB}{41,5,195}

% informações do PDF
\makeatletter
\hypersetup{
     	%pagebackref=true,
		pdftitle={\@title}, 
		pdfauthor={\@author},
    	pdfsubject={P1},
	    pdfcreator={LaTeX with abnTeX2},
		pdfkeywords={abnt}{latex}{abntex}{abntex2}{atigo científico}, 
		colorlinks=true,       		% false: boxed links; true: colored links
    	linkcolor=blue,          	% color of internal links
    	citecolor=blue,        		% color of links to bibliography
    	filecolor=magenta,      		% color of file links
		urlcolor=blue,
		bookmarksdepth=4
}
\makeatother
% --- 

% ---
% compila o indice
% ---
\makeindex
% ---

% ---
% Altera as margens padrões
% ---
\setlrmarginsandblock{1.5cm}{1.5cm}{*}
\setulmarginsandblock{1.5cm}{1.5cm}{*}
\checkandfixthelayout
% ---

% --- 
% Espaçamentos entre linhas e parágrafos 
% --- 

% O tamanho do parágrafo é dado por:
\setlength{\parindent}{0.5cm}

% Controle do espaçamento entre um parágrafo e outro:
\setlength{\parskip}{0.1cm}  % tente também \onelineskip

% Espaçamento simples
\SingleSpacing

% ----
% Início do documento
% ----
\begin{document}
\SweaveOpts{concordance=TRUE}

\thispagestyle{empty}

% Retira espaço extra obsoleto entre as frases.
\frenchspacing 

% ----------------------------------------------------------
% ELEMENTOS PRÉ-TEXTUAIS
% ----------------------------------------------------------

%---
%
% Se desejar escrever o artigo em duas colunas, descomente a linha abaixo
% e a linha com o texto ``FIM DE ARTIGO EM DUAS COLUNAS''.
% \twocolumn[    		% INICIO DE ARTIGO EM DUAS COLUNAS
%
%---
% página de titulo

%\maketitle


% ----------------------------------------------------------
% ELEMENTOS TEXTUAIS
% ----------------------------------------------------------
% É possível usar \textual ou \mainmatter, que é a macro padrão do memoir.  
\mainmatter
\begin{center}
BORGES DE MENDONÇA -- MBA EM FINANÇAS 

ANÁLISE DE INVESTIMENTOS E MERCADO DE CAPITAIS

\textbf{Professor:} Wlademir Ribeiro Prates, Dr.
\end{center}
\begin{center}
\textbf{EXERCÍCIOS DE FIXAÇÃO}
\end{center}

\noindent \textbf{Aluno:}  
\newline

%\noindent \textbf{Matrícula:} 
%\newline



% ----------------------------------------------------------
% Introdução
% ----------------------------------------------------------
%\section*{Introdução}

%\noindent  \textbf{Boa prova!}

\noindent \textbf{MERCADO FINANCEIRO}
\begin{enumerate}
\item A Comissão de Valores Mobiliários (CVM) é um  ógrão que regula e fiscaliza o mercado de capitais no Brasil, sendo:
\begin{enumerate}
\item subordinada ao Banco Central do Brasil.
\item subordinada ao Banco do Brasil.
\item subordinada à Bolsa de Valores de São Paulo (BOVESPA).
\item independente do poder público.
\item vinculada ao Ministério da Fazenda. %GABARITO
%\item \textbf{vinculada ao Ministério da Fazenda.} %GABARITO
\end{enumerate}

\item
O Banco Central do Brasil  é um órgão do Subsistema Normativo do Sistema Financeiro Nacional. Ele determina, periodicamente, a taxa de juros de referência para as operaçõoes com títulos públicos, via atuação de seu:
\begin{enumerate}
\item Comitê de Estabilidade Financeira (COMEF).
\item Comitê de Política Monetária (COPOM). %GABARITO
%\item \textbf{Comitê de Política Monetária (COPOM).} %GABARITO
\item Conselho Monetário Nacional (CMN).
\item Conselho de Administração e Câmara de Compensação de cheques e outros papéis.
\end{enumerate}

%\item Entre as atribuições do Banco Central estão:
%\begin{enumerate}
%\item emitir papel-moeda, exercer o controle do crédito e exercer a fiscalização das instituições financeiras, punindo-as quando necessário; %GABARITO
%\item determinar as taxas de recolhimento compulsório, autorizar as emissões de papel-moeda e estabelecer metas de inflação; %CMN
%\item regulamentar as operações de redesconto de liquidez, coordenar as políticas monetárias creditícia e cambial e estabelecer metas de inflação;%CMN
%\item regular o valor interno da moeda, regular o valor externo da moeda e zelar pela liquidez e solvência das instituições financeiras;%CMN
%\item determinar as taxas de recolhimento compulsório, regular o valor interno e externo da moeda e autorizar as emissões de papel-moeda.%CMN -- recolher é o BACEN, definir a taxa é o CMN.

\item É um órgão deliberativo máximo do Sistema Financeiro Nacional:
\begin{enumerate}
\item Banco Central do Brasil.
\item Conselho Monetário Nacional %GABARITO
%\item \textbf{Conselho Monetário Nacional}. %GABARITO
\item Comissão de Valores Mobiliários.
\item Conselho Nacional de Seguros Privados.
\item Banco do Brasil.
\end{enumerate}

\item É o responsável pela fiscalização das operações de Previdência Complementar Aberta:
\begin{enumerate}
\item Previc.
\item Susep. %GABARITO
%\item \textbf{Susep.} %GABARITO
\item Bacen.
\item CVM.
\end{enumerate}

\item São considerados investidores qualificados, segundo critério da CVM, as pessoas físicas e jurídica que atestem possuir investimentos financeiros superior a: 
\begin{enumerate}
\item  R\$ 300.000,00
\item  R\$ 1.000.000,00 %GABARITO -- INVESTIDOR QUALIFICADO TEM ACESSO A ALGUNS PRODUTOS EXCLUSIVOS, COM MENOS BUROCRACIA, COMO A NÃO NECESSIDADE DA INSTITUIÇÃO EMITIR PROSPECTO
%\item \textbf{R\$ 1.000.000,00} %GABARITO
\item  R\$ 500.000,00
\item  R\$ 600.000,00
\end{enumerate}

\item É responsável por conduzir a Política Monetária:
\begin{enumerate}
%\item \textbf{BACEN} %GABARITO
\item BACEN
\item CVM
\item CMN
\item ANBIMA
\end{enumerate}

\item Considere as seguintes atribuições:
\newline
I. Realizar as operações de compra e venda de ações na bolsa de valores.
\newline
 II. Estruturar operações de debêntures.
\newline
III. Conceder financiamento imobiliário.
\newline
Com relação às corretoras de títulos e valores mobiliários, está correto o que se afirma APENAS em:
\begin{enumerate}
\item I.
\item II.
\item I e II. %GABARITO
%\item \textbf{I e II.} %GABARITO
\item II e III. 
\end{enumerate}
\end{enumerate}

\noindent \textbf{CONCEITOS BÁSICOS DE FINANÇAS E GESTÃO DE RISCOS}
\begin{enumerate}
\item Dada a taxa nominal de 10\% e inflação de 4\%, qual é o cálculo para obter a taxa real: 
\begin{enumerate}
\item   $(((1,10/1,04) \times 100)- 1)$
\item $ (((1,04/1,10)-1) \times 100)$
\item  $(((1,10/1,04)-1) \times 100)$ %GABARITO
%\item  $\mathbf{(((1,10/1,04)-1) \times 100)}$%GABARITO
\item $(((1,04/1,10) \times 100)- 1)$
\end{enumerate}

\item Um investidor vendeu suas ações do setor de transporte e adquiriu no mesmo dia ações dos seguintes setores: Energia Elétrica, Alimentos, Financeiro e Varejo. Esse movimento do investidor tem como objetivo principal a redução do risco: 
\begin{enumerate}
\item soberano.
\item de crédito
\item de liquidez
\item de mercado. %GABARITO
%\item \textbf{de mercado.} %GABARITO
\end{enumerate}


\item Os retornos dos ativos A e B são apresentados na tabela a seguir:
\begin{center}
\begin{tabular}{c | c | c}
Ano & Retorno A & Retorno B \\ \toprule
1 & 11\% & 8\%\\
2 & 8\% & 11\%\\
3 & 10\% & 10\%\\
4 & 10\% & 9\%\\
5 & 9\% & 10\%\\
\end{tabular}
\end{center}
\begin{enumerate}
\item É indiferente aplicar no fundo A ou fundo B, com as informações disponíveis. %GABARITO
%\item \textbf{É indiferente aplicar no fundo A ou fundo B, com as informações disponíveis.} %GABARITO
\item  É melhor aplicar no fundo A, porque possui maior retorno esperado para o mesmo risco.
\item   É melhor aplicar no fundo B, porque para o mesmo retorno esperado possui menor risco.
\item   A escolha de aplicar no fundo A ou B depende da preferência de cada investidor, com relação ao risco e ao retorno esperados.
\end{enumerate}

\item O ativo livre de risco é aquele que:
\begin{enumerate}
\item Tem desvio-padrão dos retornos em aproximadamente zero.
\item Tem taxa de juros flutuante.
\item É mais negociado no mercado.
\item Não tem risco de crédito. %GABARITO
%\item \textbf{Não tem risco de crédito.} %GABARITO
\end{enumerate}

\item Assinale a alternativa \textbf{falsa}: 
\begin{enumerate}
\item   Se as premissas do CAPM forem aceitáveis, a carteira mais eficiente composta apenas de ativos com risco é a carteira de mercado.
\item   A carteira de mercado é aquela que possui todos os ativos do mercado, com os mesmos pesos de suas participações no mercado. Se as premissas são verdadeiras, ela está na fronteira eficiente e passa a apresentar retorno marginal decrescente em relação ao risco.
\item   O ponto no qual a CML -- \textit{Capital Market Line} -- toca a fronteira eficiente é a carteira de mercado, caso as premissas do CAPM sejam verdadeiras.
\item   O investidor opera alavancado quando diversifica sua carteira com a carteira de mercado e o ativo livre de risco, apenas utilizando recursos próprios. %GABARITO 
%\item   \textbf{O investidor opera alavancado quando diversifica sua carteira com a carteira de mercado e o ativo livre de risco, apenas utilizando recursos próprios.} %GABARITO 
\end{enumerate}
\item A forma forte de mercado eficiente é aquela em que:
\begin{enumerate}
\item Os preços históricos estão completamente refletidos nos preços atuais.
\item Todas as informações públicas estão refletidas nos preços atuais.
\item Todas as informações, inclusive as privadas, estão refletidas nos preços atuais. %GABARITO
%\item \textbf{Todas as informações, inclusive as privadas, estão refletidas nos preços atuais.} %GABARITO
\item Os retornos reais são iguais aos retornos esperados.
\end{enumerate}

\item Um cliente pergunta a um especialista em investimentos qual o retorno esperado de uma carteira de mercado considerando que a taxa livre de risco é de 2,00\% o prêmio de risco é de 4,00\% e o beta da carteira é de 1,8:
\begin{enumerate}
\item 9,8\%
\item 9,2\% %GABARITO --> E[R] = RF + (RM-RF)*B = 2 + 4*1,8 = 9,2\%
%\item \textbf{9,2\%} -- SOLUÇÃO --> $E[R] = RF + (RM-RF)\times \beta = 2 + 4\times 1,8 = 9,2\%$
\item Taxa livre de risco
\item 10,2\%
\end{enumerate}
\end{enumerate}


\noindent \textbf{INVESTIMENTOS}
\begin{enumerate}
\item É uma característica das Letras do Tesouro Nacional:
\begin{enumerate}
\item Serem indexadas a variação do dólar norte-americano.
\item  Pagarem juros calculados com base na SELIC.
\item   Pagarem juros de 6\% ao ano acrescidos da variação da TR.
\item   Terem rentabilidade prefixadas. %GABARITO
%\item   \textbf{Terem rentabilidade prefixadas.}%GABARITO
\end{enumerate}
\item As debêntures:
\begin{enumerate}
\item Possuem garantia do FGC.
\item São negociadas no SELIC.
\item Podem ou não ser conversíveis em ações. %GABARITO
%\item \textbf{Podem ou não ser conversíveis em ações.} %GABARITO
\item Representam a menor parte do capital social de uma companhia.
\end{enumerate}
\item Investidor pessoa física está isento de Imposto de Renda nas aplicações abaixo, com exceção de:
\begin{enumerate}
\item Debêntures incentivadas.
\item LCA.
\item LCI.
\item Caderneta de poupança.
%\item \textbf{CDB.} %GABARITO
\item CDB. %GABARITO
\end{enumerate}

\item Na composição de uma carteira diversificada de renda fixa para um investidor pessoa física, que exige parte do rendimento com isenção de imposto de renda retido na fonte, recomenda-se aplicação em:
\begin{enumerate}
\item   Título público do tipo Tesouro SELIC.
\item   Certificado de Recebíveis Imobiliários. %GABARITO
%\item   \textbf{Certificado de Recebíveis Imobiliários.} %GABARITO
\item   Recibo de depósito bancário.
\item   Fundos de investimento Curto Prazo.
\end{enumerate}

\item Sobre a cobrança do ``come-cotas'':
\begin{enumerate}
\item É o mesmo que IOF, e incide em CDB's e fundos de investimento para operações em até 30 dias.
\item IR que incide nos fundos de investimento pela menor alíquota da tabela regressiva nos meses de maio e novembro, com algumas exceções, como os fundos de ações. %GABARITO
%\item \textbf{IR que incide nos fundos de investimento pela menor alíquota da tabela regressiva nos meses de maio e novembro, com algumas exceções, como os fundos de ações.} %GABARITO
\item IR que incide nas vendas de ações no mercado à vista, com alíquota de 0,005\% para operações comuns e 1\% para \textit{day trade}.
\item É a taxa de administração cobrada pelas corretoras nas operações de compra de títulos públicos.
\end{enumerate}

\item Marcação de mercado é:
\begin{enumerate}
\item Prática que tem como objetivo precificar os ativos da carteira de um fundo pelo valor que os mesmos foram adquiridos.
\item Prática que não é exigida para os fundos de investimento.
\item Precificar diariamente os ativos pelo valor de mercado dos mesmos, ou seja, o valor que os mesmos podem ser vendidos e é uma responsabilidade do administrador. %GABARITO
%\item \textbf{Precificar diariamente os ativos pelo valor de mercado dos mesmos, ou seja, o valor que os mesmos podem ser vendidos e é uma responsabilidade do administrador.} %GABARITO
\item Precificar diariamente os ativos pelo valor de mercado dos mesmos, ou seja, o valor que os mesmos podem ser vendidos e é uma responsabilidade do auditor independente.
\end{enumerate}

\item São exemplos de fundos que podem cobrar taxa de performance:
\begin{enumerate}
\item   Fundo de Ações, Multimercado e Cambial. %GABARITO
%\item \textbf{Fundo de Ações, Multimercado e Cambial.} %GABARITO
\item   Fundo de Ações, Cambial e Renda Fixa 
\item   Fundo Referenciado, Multimercado e Cambial
\item   Fundo Curto Prazo, Cambial e Dívida Externa
\end{enumerate}

\end{enumerate}
\end{document}